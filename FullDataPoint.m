//
//  FullDataPoint.m
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "FullDataPoint.h"

@implementation FullDataPoint
@synthesize max,average;

- (id)initWithMinValue:(double)minVal maxValue:(double)maxVal avgValue:(double)avgVal
{
    self = [super init];
    if (self) {
        min = minVal;
        max = maxVal;
        average = avgVal;
    }
    return self;
}

-(NSString*) description{
    return [NSString stringWithFormat:@"Min:%f Max:%f Avg:%f",min,max,average];
}

@end
