//
//  MutableDataPoint.m
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "MutableDataPoint.h"

@implementation MutableDataPoint

- (id)initWithMinValue:(double)minVal maxValue:(double)maxVal avgValue:(double)avgVal
{
    self = [super initWithMinValue:minVal maxValue:maxVal avgValue:avgVal];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (id)init
{
    return [self initWithMinValue:0 maxValue:0 avgValue:0];
}

- (void) setMax:(double)maxVal{
    max = maxVal;
}                               

- (void) setMin:(double)minVal{                                   
    min = minVal;
}

- (void) setAverage:(double)avgVal{
    average = avgVal;
}

@end
