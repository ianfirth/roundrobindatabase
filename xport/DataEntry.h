//
//  DataEntry.h
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

@interface DataEntry : NSObject{
    NSString* __strong _name;
    NSMutableArray* __strong _values;
    NSString* __strong _valueType;
    NSString* __strong _objectType;
    NSString* __strong _objectId;
    NSString* __strong _propertyDescription;
}

- (id)initWithName:(NSString*)name;
// full name
@property (readonly) NSString* name;
// average, Max , Min etc.
@property (readonly) NSString* valueType; 
// Host, VM
@property (readonly) NSString* objectType; 
// object idefnifier
@property (readonly) NSString* objectId;
// property descriptor
@property (readonly) NSString* propertyDescription;

@property (readonly) NSArray* values;

@end
