//
//  xportDatabase.h
//  xportDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

@interface XportDatabase : NSObject<NSXMLParserDelegate>{
    // value of current element
    NSMutableString *elementValue;
    
    //dataEntries
    NSMutableArray* _dataEntries;
    
    NSDate* _start;
    
    NSDate* _end;
    
    int _step;
    
    int vCounter;
}

- (id)initWithString:(NSString *)xmlString;
- (id)initWithUrl:(NSURL *)url;
// designated initializer
- (id)initWithData:(NSData *)data;

@property (readonly, strong) NSDate *startDate;
@property (readonly, strong) NSDate *endDate;
@property (readonly) int step;
@property (readonly, strong) NSArray* dataEntries;


// columns is the number of entires
// rows is the array of values for each entry

@end
