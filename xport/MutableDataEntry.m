//
//  MutableDataEntry.m
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "MutableDataEntry.h"

@implementation MutableDataEntry


- (void) AddValue:(double)val{
    if (!_values){
        _values = [[NSMutableArray alloc] init];
    }
    //NSLog(@"adding %f to %@",val,[self name]);
    [_values addObject:[[NSNumber alloc] initWithDouble:val]];
}

@end
