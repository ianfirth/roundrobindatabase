//
//  DataEntry2.m
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "DataEntry.h"

@implementation DataEntry

@synthesize name = _name;
@synthesize values = _values;
@synthesize valueType = _valueType ;
@synthesize objectType = _objectType;
@synthesize objectId = _objectId;
@synthesize propertyDescription = _propertyDescription ;

- (id)initWithName:(NSString*)name
{
    self = [super init];
    if (self) {
        _name = name;
        // parse the name into individual values
        // e.g. AVERAGE:host:8227c277-ff26-4414-81a7-bff2f7a66be3:memory_total_kib
        // valueType = AVERAGE
        // ObjectType = host
        // ObjectID = 8227c277-ff26-4414-81a7-bff2f7a66be3
        // PropertyDescription = memory_total_kib
        
        NSArray* parts = [_name componentsSeparatedByString:@":"];
        if ([parts count] == 4){
            _valueType = [parts objectAtIndex:0];
            _objectType  = [parts objectAtIndex:1];
            _objectId = [parts objectAtIndex:2];
            _propertyDescription  = [parts objectAtIndex:3];
        }

    }
    return self;
}

@end
