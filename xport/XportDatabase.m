//
//  XportDatabase.m
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XportDatabase.h"
#import "MutableDataEntry.h"

@implementation XportDatabase

@synthesize step = _step,
            startDate = _start,
            endDate = _end,
            dataEntries = _dataEntries;

- (id)initWithString:(NSString *)xmlString{
    return [self initWithData:[xmlString dataUsingEncoding:NSUTF8StringEncoding]];
}

- (id)initWithUrl:(NSURL *)url{
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    return [self initWithData:data];         
}

// designated initializer
- (id)initWithData:(NSData *)data
{
    self = [super init];
    if (self) {
        vCounter = 0;
        _dataEntries = [[NSMutableArray alloc] init];
        NSXMLParser* xmlParser = [[NSXMLParser alloc] initWithData:data];
        [xmlParser setDelegate:self];
        BOOL parsedOK = [xmlParser parse];
        xmlParser = nil;
        if (!parsedOK){
            [NSException raise:@"Failed to Parse XPORT" format:@"XML Parse error %@", [xmlParser parserError]];
        }
    }
    return self;
}


-(NSArray*)dataSources{
    return _dataEntries;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict{
    
    // clear out any values from previous element
    elementValue = nil;
    // if 'row' reset vCounter
    if ([elementName isEqualToString:@"row"]){
        vCounter = 0;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if (!elementValue){
        elementValue = [[NSMutableString alloc] initWithString:string];
    }
    else{
        [elementValue appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    // if element ending is 'start' then store it in the Xport
    if ([elementName isEqualToString:@"start"]){
        _start = [NSDate dateWithTimeIntervalSince1970:[elementValue intValue]];
    }

    // if element ending is 'end' then store it in the Xport
    if ([elementName isEqualToString:@"end"]){
        _end = [NSDate dateWithTimeIntervalSince1970:[elementValue intValue]];   
    }

    // if element ending is 'step' then store it in the Xport
    if ([elementName isEqualToString:@"step"]){
        _step = [elementValue intValue];
    }

    // if element ending is 'entry' then create a dataentry and add it to the entries array
    if ([elementName isEqualToString:@"entry"]){
        MutableDataEntry* entry = [[MutableDataEntry alloc] initWithName:elementValue];
        [_dataEntries addObject:entry];
    }
    
    // if it is a 'v' then take the value and add it to the entiry at index of 'vCount'
    if ([elementName isEqualToString:@"v"]){
        MutableDataEntry* dataEntry = [_dataEntries objectAtIndex:vCounter];
        [dataEntry AddValue:[elementValue doubleValue]];
        vCounter ++;
    }
}

@end
