//
//  RoundRobinDatabase.m
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//#import "RoundRobinDatabase.h"
#import "MutableDataSource.h"
#import "MutableDataPoint.h"

@implementation RoundRobinDatabase
@synthesize step,lastUpdate;

- (id)initWithString:(NSString *)xmlString  withFullDataSet:(BOOL)fullDataSet{
    return [self initWithData:[xmlString dataUsingEncoding:NSUTF8StringEncoding] withFullDataSet:fullDataSet];
}

- (id)initWithUrl:(NSURL *)url withFullDataSet:(BOOL)fullDataSet{
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    return [self initWithData:data withFullDataSet:fullDataSet];         
}

// designated initializer
- (id)initWithData:(NSData *)data withFullDataSet:(BOOL)fullDataSet
{
    self = [super init];
    if (self) {
        useFullDataSet = fullDataSet;
        elementList = [[NSMutableArray alloc] initWithCapacity:3];
        _dataSources = [[NSMutableArray alloc] init];
        NSXMLParser* xmlParser = [[NSXMLParser alloc] initWithData:data];
        [xmlParser setDelegate:self];
        BOOL parsedOK = [xmlParser parse];
        xmlParser = nil;
        if (!parsedOK){
            [NSException raise:@"Failed to Parse RRD" format:@"XML Parse error %@", [xmlParser parserError]];
        }
    }
    return self;
}

- (void) dealloc{
    elementList = nil;
    if (elementValue){
        elementValue = nil;
    }
    
    _dataSources = nil;
    DataSourceObjectParams = nil;
}

-(NSArray*)dataSources{
    return _dataSources;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict{

    // add the element to the end of the list
    [elementList addObject:elementName];
    
    // if this element is one that contains values that need to be used for an object clear the store ready for its values
    if ([elementName isEqualToString:@"ds"]){
        if (DataSourceObjectParams){
            DataSourceObjectParams = nil;
        }
        DataSourceObjectParams = [[NSMutableDictionary alloc] initWithCapacity:2];
    }

    // if this element is an rra then clear the information relating to the step consolidate
    // and the consolidation type
    if ([elementName isEqualToString:@"rra"]){
        consolidationType = nil;
    }

    if ([elementName isEqualToString:@"row"]){
        dataSourceValueCounter =0;
    }

    if ([elementName isEqualToString:@"database"]){
        rowCounter =0;
    }

    elementValue = nil;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if (!elementValue){
        elementValue = [[NSMutableString alloc] initWithString:string];
    }
    else{
        [elementValue appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{

    // populate the step element
    if ([[elementList lastObject] isEqualToString:@"step"]  &&  [[elementList objectAtIndex:[elementList count]-2] isEqualToString:@"rrd"]){
        step = [elementValue intValue];
    }

    // populate the date time that the last element was entered into the database
    if ([[elementList lastObject] isEqualToString:@"lastupdate"]  &&  [[elementList objectAtIndex:[elementList count]-2] isEqualToString:@"rrd"]){
        // construct NSDate from Epoch
        lastUpdate = [NSDate dateWithTimeIntervalSince1970:[elementValue intValue]];
    }

    // populate the dataSource values
    if ([elementList count] > 1 && [[elementList objectAtIndex:[elementList count]-2] isEqualToString:@"ds"]){
        [DataSourceObjectParams setValue:elementValue forKey:[elementList lastObject]];
    }

    // create the datasource and add to rrd
    if ( [elementList count] > 1 &&  
         [[elementList objectAtIndex:[elementList count]-2] isEqualToString:@"rrd"] && 
         [[elementList lastObject] isEqualToString:@"ds"]){
       // construct the dataSource object
        NSString* name = [DataSourceObjectParams valueForKey:@"name"];
        NSString* dsType = [DataSourceObjectParams valueForKey:@"type"];
        //int max = [[objectParams valueForKey:@"max"] intValue];
        //int min = [[objectParams valueForKey:@"min"] intValue];
        //int minHeartbeat = [[objectParams valueForKey:@"minimalHeartbeat"] intValue];
        MutableDataSource* ds =  [[MutableDataSource alloc] initWithName:name Type:dsType ForDatabase:self];
        [_dataSources addObject:ds];
        ds = nil;
    }

    // create the datasource and add to rrd
    if ([[elementList lastObject] isEqualToString:@"cf"]){
        consolidationType = [elementValue copy];
    }

    if ([[elementList lastObject] isEqualToString:@"pdp_per_row"]){
        stepConsolodation = [elementValue intValue];
    }

    if ([[elementList lastObject] isEqualToString:@"v"]){
        //NSLog(@"RowCount=%@",(rowCounter));
        double value = [elementValue doubleValue];
        // add the value to the approprriate dataPoint
        MutableDataSource* dataSource = [_dataSources objectAtIndex:dataSourceValueCounter];
        // rowCounter is the position in the dataSet
        NSArray* dataPoints = [dataSource dataPointsForConsolodatedStepCount:stepConsolodation];

        // create and add the point to the source
        if ([dataPoints count] <= (rowCounter)){
            if (useFullDataSet){
                MutableDataPoint* dataPoint = [[MutableDataPoint alloc] init];
                [dataSource addDataPoint:dataPoint forStepCount:stepConsolodation];
                dataPoint = nil;
            }
        }

        dataPoints = [dataSource dataPointsForConsolodatedStepCount:stepConsolodation];
        MutableDataPoint* dp = nil;
        if ([dataPoints count] > rowCounter){
            dp = [dataPoints objectAtIndex:(rowCounter)];
        }
        
        if ([consolidationType isEqualToString:@"MAX"]){
            if (dp && [dp isKindOfClass:[MutableDataPoint class]]){
                [dp setMax:value];
            }
        }
        else if ([consolidationType isEqualToString:@"MIN"]){
            if (dp && [dp isKindOfClass:[MutableDataPoint class]]){
             [dp setMin:value];   
            }
        }
        else if ([consolidationType isEqualToString:@"AVERAGE"]){
            if (dp && [dp isKindOfClass:[MutableDataPoint class]]){
                [dp setAverage:value];
            }
            else if (stepConsolodation ==1){
                DataPoint* dataPoint = [[DataPoint alloc] initWitValue:value];
                [dataSource addDataPoint:dataPoint forStepCount:stepConsolodation];
                dataPoint = nil;
            }
            else if (stepConsolodation !=1){
                DataPoint* dataPoint = [[DataPoint alloc] initWitValue:value];
                [dataSource addDataPoint:dataPoint forStepCount:stepConsolodation];
                dataPoint = nil;
            }

        }
        // dataSourceValue counter is the dataSource number in the rrd
        dataSourceValueCounter++;
    }
    
    if ([[elementList lastObject] isEqualToString:@"row"]){        
        rowCounter++;
    }

    elementValue = nil;

    [elementList removeLastObject];
}
@end
