//
//  RoundRobinDatabaseTests.m
//  RoundRobinDatabaseTests
//
//  Created by Ian Firth on 02/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoundRobinDatabaseTests.h"
#import "RoundRobinDatabase.h"
#import "DataSource.h"
#import "FullDataPoint.h"
#import "XportDatabase.h"
#import "DataEntry.h"

@implementation RoundRobinDatabaseTests

- (void)setUp
{
    [super setUp];
    xmlString = [[NSMutableString alloc] initWithString:@"<rrd>"];
    [xmlString appendString:@"<version>0003</version><step>5</step><lastupdate>1314950342</lastupdate>"];
    // define the dataSources
    // add ds
    [xmlString appendString:@"<ds><name>memory_total_kib</name><type>GAUGE</type><minimal_heartbeat>300.0000</minimal_heartbeat><min>0.0</min><max>Infinity</max><last_ds>12580412</last_ds><value>31522535.7903</value><unknown_sec>0</unknown_sec></ds>"];
    // add ds
    [xmlString appendString:@"<ds><name>memory_free_kib</name><type>GAUGE</type><minimal_heartbeat>300.0000</minimal_heartbeat><min>0.0</min><max>Infinity</max><last_ds>9248</last_ds><value>23172.5647</value><unknown_sec>0</unknown_sec></ds>"];
    
    // define the Archives
    // define an Average value archive
    [xmlString appendString:@"<rra>"];
    [xmlString appendString:@"<cf>AVERAGE</cf><pdp_per_row>1</pdp_per_row><params><xff>0.5000</xff></params>"];
    [xmlString appendString:@"<cdp_prep>"];
    [xmlString appendString:@"<ds><primary_value>0.0</primary_value><secondary_value>0.0</secondary_value><value>0.0</value><unknown_datapoints>0</unknown_datapoints></ds>"];
    [xmlString appendString:@"<ds><primary_value>0.0</primary_value><secondary_value>0.0</secondary_value><value>0.0</value><unknown_datapoints>0</unknown_datapoints></ds>"];
    [xmlString appendString:@"</cdp_prep>"];
    [xmlString appendString:@"<database>"];
    [xmlString appendString:@"<row>"];  // must be same number of values ih here are dataSources
    [xmlString appendString:@"<v>12580412.0000</v>"];
    [xmlString appendString:@"<v>9248.0000</v>"];
    [xmlString appendString:@"</row>"];
    [xmlString appendString:@"<row>"];  // must be same number of values ih here are dataSources
    [xmlString appendString:@"<v>1.5000</v>"];
    [xmlString appendString:@"<v>.5000</v>"];
    [xmlString appendString:@"</row>"];
    [xmlString appendString:@"</database>"];
    [xmlString appendString:@"</rra>"];
    
    // define A Max value archive
    [xmlString appendString:@"<rra>"];
    [xmlString appendString:@"<cf>MAX</cf><pdp_per_row>1</pdp_per_row><params><xff>0.5000</xff></params>"];
    [xmlString appendString:@"<cdp_prep>"];
    [xmlString appendString:@"<ds><primary_value>0.0</primary_value><secondary_value>0.0</secondary_value><value>0.0</value><unknown_datapoints>0</unknown_datapoints></ds>"];
    [xmlString appendString:@"<ds><primary_value>0.0</primary_value><secondary_value>0.0</secondary_value><value>0.0</value><unknown_datapoints>0</unknown_datapoints></ds>"];
    [xmlString appendString:@"</cdp_prep>"];
    [xmlString appendString:@"<database>"];
    [xmlString appendString:@"<row>"];  // must be same number of values ih here are dataSources
    [xmlString appendString:@"<v>1000</v>"];
    [xmlString appendString:@"<v>1000</v>"];
    [xmlString appendString:@"</row>"];
    [xmlString appendString:@"<row>"];  // must be same number of values ih here are dataSources
    [xmlString appendString:@"<v>1000</v>"];
    [xmlString appendString:@"<v>1000</v>"];
    [xmlString appendString:@"</row>"];
    [xmlString appendString:@"</database>"];
    [xmlString appendString:@"</rra>"];
    
    // define A Min value archive
    [xmlString appendString:@"<rra>"];
    [xmlString appendString:@"<cf>MIN</cf><pdp_per_row>1</pdp_per_row><params><xff>0.5000</xff></params>"];
    [xmlString appendString:@"<cdp_prep>"];
    [xmlString appendString:@"<ds><primary_value>0.0</primary_value><secondary_value>0.0</secondary_value><value>0.0</value><unknown_datapoints>0</unknown_datapoints></ds>"];
    [xmlString appendString:@"<ds><primary_value>0.0</primary_value><secondary_value>0.0</secondary_value><value>0.0</value><unknown_datapoints>0</unknown_datapoints></ds>"];
    [xmlString appendString:@"</cdp_prep>"];
    [xmlString appendString:@"<database>"];
    [xmlString appendString:@"<row>"];  // must be same number of values ih here are dataSources
    [xmlString appendString:@"<v>0.5</v>"];
    [xmlString appendString:@"<v>0.5</v>"];
    [xmlString appendString:@"</row>"];
    [xmlString appendString:@"<row>"];  // must be same number of values ih here are dataSources
    [xmlString appendString:@"<v>0.5</v>"];
    [xmlString appendString:@"<v>0.5</v>"];
    [xmlString appendString:@"</row>"];
    [xmlString appendString:@"</database>"];
    [xmlString appendString:@"</rra>"];
    
    // define an Average value archive for a consolidation step of 1000
    [xmlString appendString:@"<rra>"];
    [xmlString appendString:@"<cf>AVERAGE</cf><pdp_per_row>1000</pdp_per_row><params><xff>0.5000</xff></params>"];
    [xmlString appendString:@"<cdp_prep>"];
    [xmlString appendString:@"<ds><primary_value>0.0</primary_value><secondary_value>0.0</secondary_value><value>0.0</value><unknown_datapoints>0</unknown_datapoints></ds>"];
    [xmlString appendString:@"<ds><primary_value>0.0</primary_value><secondary_value>0.0</secondary_value><value>0.0</value><unknown_datapoints>0</unknown_datapoints></ds>"];
    [xmlString appendString:@"</cdp_prep>"];
    [xmlString appendString:@"<database>"];
    [xmlString appendString:@"<row>"];  // must be same number of values ih here are dataSources
    [xmlString appendString:@"<v>500</v>"];
    [xmlString appendString:@"<v>500</v>"];
    [xmlString appendString:@"</row>"];
    [xmlString appendString:@"<row>"];  // must be same number of values ih here are dataSources
    [xmlString appendString:@"<v>500</v>"];
    [xmlString appendString:@"<v>500</v>"];
    [xmlString appendString:@"</row>"];
    [xmlString appendString:@"</database>"];
    [xmlString appendString:@"</rra>"];
    
    // close the database
    [xmlString appendString:@"</rrd>"];
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];
}

- (void) testInvalidXML{
    // build rrd file
    NSMutableString* xmlString2 = [[NSMutableString alloc] initWithString:@"<rrd>"];
        
    // validate that the creation throws an exception
    STAssertThrows([[RoundRobinDatabase alloc] initWithString:xmlString2  withFullDataSet:YES], @"Expected to detect invalidXML");
}

// A general test with ValidXML that checks a known good path works
// there is no attempt to test all combination or limits of the values here
// separate tests should be provided for these limit tests
- (void) testValidXMLGeneralFullBuild{
    // construct the class tree
    RoundRobinDatabase* rrd = [[RoundRobinDatabase alloc] initWithString:xmlString withFullDataSet:YES];    

    // validate the the properties have been popultated correctly
    STAssertEquals([rrd step],
                   5,
                   @"Step is expected to be 5");
    
    // validate that there is the correct number of dataSources
    STAssertTrue([[rrd dataSources] count] == 2,
                   @"Should be 2 DataSources");
    

    // validate that the datasource name and orders are maintained
    DataSource* ds1 = [[rrd dataSources] objectAtIndex:0];
    DataSource* ds2 = [[rrd dataSources] objectAtIndex:1];

    STAssertEqualObjects([ds1 name],
                   @"memory_total_kib",
                   [NSString  stringWithFormat:@"DataSource should be memory_total_kib but was %@",[ds1 name]]);

    STAssertEqualObjects([ds2 name],
                   @"memory_free_kib",
                   [NSString  stringWithFormat:@"DataSource should be memory_free_kib but was %@",[ds2 name]]);

    
    // validate that the datSource contains all the correct data
    STAssertEqualObjects([ds1 name],
                   @"memory_total_kib",
                   [NSString  stringWithFormat:@"DataSource should be memory_total_kib but was %@",[ds1 name]]);

    STAssertEquals([ds1 dsType],
                   DS_Gauge,
                   @"DataStrore 1 type was expected to be GAUGE but wass not.");


    // check that the datasets have the correct available granlarities of data available
    STAssertTrue([[ds1 availableSteps] count] == 2, @"Expected 2 sets of data for dataSource 1");
    STAssertTrue([[ds2 availableSteps] count] == 2, @"Expected 2 sets of data for dataSource 2");
    
    //NSNumber* consolodation1 = [NSNumber
    // check the number of datapoints in each dataSource and the max/min/average values
    
    STAssertTrue([[ds1 dataPointsForConsolodatedStepCount:1] count] == 2, @"Expected 4 dataPoints in each dataSource");
    DataPoint* dp1 = [[ds1 dataPointsForConsolodatedStepCount:1] objectAtIndex:0];
    STAssertTrue([dp1 min] == 0.5, @"Expected point 1 min to be 0.5");
    STAssertTrue([dp1 max] == 1000, @"Expected point 1 max to be 1000");
    STAssertTrue([dp1 average] == 12580412.0000, @"Expected point 1 Average to be 12580412.0000");

    DataPoint* dp2 = [[ds1 dataPointsForConsolodatedStepCount:1000] objectAtIndex:0];
    STAssertTrue([dp2 min] == 0, @"Expected point 3 min to be 0");
    STAssertTrue([dp2 max] == 0, @"Expected point 3 max to be 0");
    STAssertTrue([dp2 average] == 500, @"Expected point 3 Average to be 500");
    
    dp2 = [[ds1 dataPointsForConsolodatedStepCount:1000] objectAtIndex:1];
    STAssertTrue([dp2 min] == 0, @"Expected point 3 min to be 0");
    STAssertTrue([dp2 max] == 0, @"Expected point 3 max to be 0");
    STAssertTrue([dp2 average] == 500, @"Expected point 3 Average to be 500");

    NSDictionary* datapoints = [ds1 allDataPointsWithStepConsolidation:1];
    STAssertTrue([datapoints count] == 2 , @"expected to be 2 datapoints in plot dictionary");
    // check times and values
    NSArray* sortedDatesArray = [datapoints allKeys];

    int position = 0;
    for (NSDate* date in sortedDatesArray){
        DataPoint* dp = [datapoints objectForKey:date];
        if (position == 0){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342)];
            STAssertTrue([date isEqualToDate:exp], @"latest date - 5 sec");
            STAssertEquals([dp average], 12580412.0 , @"Average = 12580412.0");
        }
        if (position == 1){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342-5)];
            STAssertTrue([date isEqualToDate:exp], @"latest date");
            STAssertEquals([dp average], 1.5 , @"Average = 1.5");
        }
        position ++;
    }
    
    datapoints = [ds1 allDataPointsWithStepConsolidation:1000];
    STAssertTrue([datapoints count] == 2 , @"expected to be 2 datapoints in plot dictionary");
    position = 0;
    sortedDatesArray = [datapoints allKeys];
    
    for (NSDate* date in sortedDatesArray){
        DataPoint* dp = [datapoints objectForKey:date];
        if (position == 1){
            // step = 5 consolodation = 1000 (therefore 5000 seconds to next point)
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342-5000)];
            STAssertTrue([date isEqualToDate:exp], @"latest date - 5000 sec");
            STAssertEquals([dp average], 500.0 , @"Average = 500.0");
        }
        if (position == 0){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:1314950342];
            STAssertTrue([date isEqualToDate:exp], @"latest date");
            STAssertEquals([dp average], 500.0 , @"Average = 500.0");
        }
        position ++;
    }

    
    datapoints = [ds1 allDataPointsWithBestStepsGranularity];
    STAssertTrue([datapoints count] == 3 , @"expected to be 3 datapoints in plot dictionary");
    position = 0;
    
    sortedDatesArray = [datapoints allKeys];
                         
    for (NSDate* date in sortedDatesArray){
        DataPoint* dp = [datapoints objectForKey:date];
        if (position == 2){
            // step = 5 consolodation = 1000 (therefore 5000 seconds to next point)
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342-5000)];
            STAssertTrue([date isEqualToDate:exp], @"latest date - 5000 sec");
            STAssertEquals([dp average], 500.0 , @"Average = 500.0");
        }
        if (position == 1){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342-5)];
            STAssertTrue([date isEqualToDate:exp], @"latest date");
            STAssertEquals([dp average], 1.5 , @"Average = 1.5");
        }
        if (position == 0){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:1314950342];
            STAssertTrue([date isEqualToDate:exp], @"latest date");
            STAssertEquals([dp average], 12580412.0 , @"Average = 12580412");
        }
        position ++;
    }
}

// A general test with ValidXML that checks a known good path works
// there is no attempt to test all combination or limits of the values here
// separate tests should be provided for these limit tests
- (void) testValidXMLGeneralMinimalBuild{
    // construct the class tree
    RoundRobinDatabase* rrd = [[RoundRobinDatabase alloc] initWithString:xmlString withFullDataSet:NO];    
    
    // validate the the properties have been popultated correctly
    STAssertEquals([rrd step],
                   5,
                   @"Step is expected to be 5");
    
    // validate that there is the correct number of dataSources
    STAssertTrue([[rrd dataSources] count] == 2,
                 @"Should be 2 DataSources");
    
    
    // validate that the datasource name and orders are maintained
    DataSource* ds1 = [[rrd dataSources] objectAtIndex:0];
    DataSource* ds2 = [[rrd dataSources] objectAtIndex:1];
    
    STAssertEqualObjects([ds1 name],
                         @"memory_total_kib",
                         [NSString  stringWithFormat:@"DataSource should be memory_total_kib but was %@",[ds1 name]]);
    
    STAssertEqualObjects([ds2 name],
                         @"memory_free_kib",
                         [NSString  stringWithFormat:@"DataSource should be memory_free_kib but was %@",[ds2 name]]);
    
    
    // validate that the datSource contains all the correct data
    STAssertEqualObjects([ds1 name],
                         @"memory_total_kib",
                         [NSString  stringWithFormat:@"DataSource should be memory_total_kib but was %@",[ds1 name]]);
    
    STAssertEquals([ds1 dsType],
                   DS_Gauge,
                   @"DataStrore 1 type was expected to be GAUGE but wass not.");
    
    
    // check that the datasets have the correct available granlarities of data available
    STAssertTrue([[ds1 availableSteps] count] == 2, @"Expected 2 sets of data for dataSource 1");
    STAssertTrue([[ds2 availableSteps] count] == 2, @"Expected 2 sets of data for dataSource 2");
    
    //NSNumber* consolodation1 = [NSNumber
    // check the number of datapoints in each dataSource and the max/min/average values
    
    STAssertTrue([[ds1 dataPointsForConsolodatedStepCount:1] count] == 2, @"Expected 4 dataPoints in each dataSource");
    DataPoint* dp1 = [[ds1 dataPointsForConsolodatedStepCount:1] objectAtIndex:0];
    STAssertTrue([dp1 min] == 12580412.0, @"Expected point 1 min to be 12580412.0");
    STAssertTrue([dp1 max] == 12580412.0, @"Expected point 1 max to be 12580412.0");
    STAssertTrue([dp1 average] == 12580412.0, @"Expected point 1 Average to be 12580412.0");
    
    DataPoint* dp2 = [[ds1 dataPointsForConsolodatedStepCount:1000] objectAtIndex:0];
    STAssertTrue([dp2 min] == 500.0, @"Expected point 3 min to be 500");
    STAssertTrue([dp2 max] == 500.0, @"Expected point 3 max to be 500");
    STAssertTrue([dp2 average] == 500.0, @"Expected point 3 Average to be 500");
    
    dp2 = [[ds1 dataPointsForConsolodatedStepCount:1000] objectAtIndex:1];
    STAssertTrue([dp2 min] == 500.0, @"Expected point 3 min to be 500");
    STAssertTrue([dp2 max] == 500.0, @"Expected point 3 max to be 500");
    STAssertTrue([dp2 average] == 500.0, @"Expected point 3 Average to be 500");
    
    NSDictionary* datapoints = [ds1 allDataPointsWithStepConsolidation:1];
    STAssertTrue([datapoints count] == 2 , @"expected to be 2 datapoints in plot dictionary");
    // check times and values
    NSArray* sortedDatesArray = [datapoints allKeys];
    
    int position = 0;
    for (NSDate* date in sortedDatesArray){
        DataPoint* dp = [datapoints objectForKey:date];
        if (position == 0){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342)];
            STAssertTrue([date isEqualToDate:exp], @"latest date - 5 sec");
            STAssertEquals([dp average], 12580412.0 , @"Average = 12580412.0");
        }
        if (position == 1){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342-5)];
            STAssertTrue([date isEqualToDate:exp], @"latest date");
            STAssertEquals([dp average], 1.5 , @"Average = 1.5");
        }
        position ++;
    }
    
    datapoints = [ds1 allDataPointsWithStepConsolidation:1000];
    STAssertTrue([datapoints count] == 2 , @"expected to be 2 datapoints in plot dictionary");
    position = 0;
    sortedDatesArray = [datapoints allKeys];
    
    for (NSDate* date in sortedDatesArray){
        DataPoint* dp = [datapoints objectForKey:date];
        if (position == 1){
            // step = 5 consolodation = 1000 (therefore 5000 seconds to next point)
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342-5000)];
            STAssertTrue([date isEqualToDate:exp], @"latest date - 5000 sec");
            STAssertEquals([dp average], 500.0 , @"Average = 500.0");
        }
        if (position == 0){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:1314950342];
            STAssertTrue([date isEqualToDate:exp], @"latest date");
            STAssertEquals([dp average], 500.0 , @"Average = 500.0");
        }
        position ++;
    }
    
    
    datapoints = [ds1 allDataPointsWithBestStepsGranularity];
    STAssertTrue([datapoints count] == 3 , @"expected to be 3 datapoints in plot dictionary");
    position = 0;
    
    sortedDatesArray = [datapoints allKeys];
    
    for (NSDate* date in sortedDatesArray){
        DataPoint* dp = [datapoints objectForKey:date];
        if (position == 2){
            // step = 5 consolodation = 1000 (therefore 5000 seconds to next point)
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342-5000)];
            STAssertTrue([date isEqualToDate:exp], @"latest date - 5000 sec");
            STAssertEquals([dp average], 500.0 , @"Average = 500.0");
        }
        if (position == 1){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:(1314950342-5)];
            STAssertTrue([date isEqualToDate:exp], @"latest date");
            STAssertEquals([dp average], 1.5 , @"Average = 1.5");
        }
        if (position == 0){
            NSDate* exp = [NSDate dateWithTimeIntervalSince1970:1314950342];
            STAssertTrue([date isEqualToDate:exp], @"latest date");
            STAssertEquals([dp average], 12580412.0 , @"Average = 12580412.0");
        }
        position ++;
    }
}

// this test just checks that a complicated RRD can be loaded OK
-(void) testLoadDatabaseFromFile{    
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString* currentPath = [manager currentDirectoryPath];
    // currentPath should be something like  /Users/ianfirth/Documents/xcode/RoundRobinDatabase/RoundRobinDatabase
    // add RoundRobinDatabaseTests/XenRRDHost.xml on the end so path becomes
    // /Users/ianfirth/Documents/xcode/RoundRobinDatabase/RoundRobinDatabase/RoundRobinDatabaseTestsTests/XenRRDHost.xml
    
    NSString* filePath = [NSString stringWithFormat:@"%@/RoundRobinDatabaseTests/XenRRDHost.xml", currentPath];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    @try {
        RoundRobinDatabase* db = [[RoundRobinDatabase alloc] initWithData:myData withFullDataSet:YES];
        NSArray* dataSources = [db dataSources];
        int dataSourceCount = [dataSources count];
        STAssertEquals(dataSourceCount, 26, @"Expected 25 datasources");
        DataSource* source = [[db dataSources] objectAtIndex:0];
        int steps = [[source availableSteps] count];
        STAssertEquals(steps, 4, @"Expected 4 consolidation steps for the datasources");
    }
    @catch (NSException *exception) {
        STFail(@"Failed to load database fron file");
    }
}

-(void) testLoadDatabaseFromFileMinimal{    
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString* currentPath = [manager currentDirectoryPath];
    // currentPath should be something like  /Users/ianfirth/Documents/xcode/RoundRobinDatabase/RoundRobinDatabase
    // add RoundRobinDatabaseTests/XenRRDHost.xml on the end so path becomes
    // /Users/ianfirth/Documents/xcode/RoundRobinDatabase/RoundRobinDatabase/RoundRobinDatabaseTestsTests/XenRRDHost.xml
    
    NSString* filePath = [NSString stringWithFormat:@"%@/RoundRobinDatabaseTests/XenRRDHost.xml", currentPath];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    @try {
        RoundRobinDatabase* db = [[RoundRobinDatabase alloc] initWithData:myData withFullDataSet:NO];
        NSArray* dataSources = [db dataSources];
        int dataSourceCount = [dataSources count];
        STAssertEquals(dataSourceCount, 26, @"Expected 25 datasources");
        DataSource* source = [[db dataSources] objectAtIndex:0];
        int steps = [[source availableSteps] count];
        STAssertEquals(steps, 4, @"Expected 4 consolidation steps for the datasources");
    }
    @catch (NSException *exception) {
        STFail(@"Failed to load database fron file");
    }
}

// add a separate test for all the ds types here
-(void) testFullDataPointDescription{
    FullDataPoint* dp = [[FullDataPoint alloc] initWithMinValue:2 maxValue:3 avgValue:4];
    NSString* desc = [dp description];
    NSString* expected = [NSString stringWithFormat:@"Min:%f Max:%f Avg:%f",2.0,3.0,4.0];
    STAssertEqualObjects(desc,
                         expected,
                         @"DataPoint Description was not as expected");
}

// add a separate test for all the ds types here
-(void) testDataPointDescription{
    DataPoint* dp = [[DataPoint alloc] initWitValue:4.0];
    NSString* desc = [dp description];
    NSString* expected = [NSString stringWithFormat:@"Value:%f",4.0];
    STAssertEqualObjects(desc,
                         expected,
                         @"DataPoint Description was not as expected");
}

-(void) testXenHostFile{
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString* currentPath = [manager currentDirectoryPath];    
    NSString* filePath = [NSString stringWithFormat:@"%@/RoundRobinDatabaseTests/XenRRDHost.xml", currentPath];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    @try {
         RoundRobinDatabase* db = [[RoundRobinDatabase alloc] initWithData:myData  withFullDataSet:YES];
        NSArray* dataSources = [db dataSources];
        int dataSourceCount = [dataSources count];
        STAssertEquals(dataSourceCount, 26, @"Expected 26 datasources");

        DataSource* selectedDataSource = [[db dataSources] objectAtIndex:17];
        NSDictionary* dataPoints = [selectedDataSource allDataPointsWithStepConsolidation:1];
        NSArray* sortedDatesArray = [dataPoints allKeys];
        // check that there are the expected set of datapoints
        int dataPointCount = [sortedDatesArray count];
        STAssertEquals(dataPointCount,120, @"Expected 120 datapoints");
        dataPoints = [selectedDataSource allDataPointsWithStepConsolidation:12];
        // check that there are the expected set of datapoints
        sortedDatesArray = [dataPoints allKeys];
        // check that there are the expected set of datapoints
        dataPointCount = [sortedDatesArray count];
        STAssertEquals(dataPointCount,120, @"Expected 120 datapoints");
        // check that all dates are getting newer (i.e. dictionary is sorted by default
        NSDate* currentDate = [sortedDatesArray objectAtIndex:0];
        for (NSDate* date in sortedDatesArray){
            if ([date compare:currentDate] == NSOrderedDescending){
               STFail(@"Expected dates to be Ascending (i.e. latest date first");
            }
            else{
                currentDate = date;
            }
        }
        
        dataPoints = [selectedDataSource allDataPointsWithStepConsolidation:720];
        // check that there are the expected set of datapoints
        sortedDatesArray = [dataPoints allKeys];
        // check that there are the expected set of datapoints
        dataPointCount = [sortedDatesArray count];
        STAssertEquals(dataPointCount,168, @"Expected 168 datapoints");
        dataPoints = [selectedDataSource allDataPointsWithStepConsolidation:17280];
        // check that there are the expected set of datapoints
        sortedDatesArray = [dataPoints allKeys];
        // check that there are the expected set of datapoints
        dataPointCount = [sortedDatesArray count];
        STAssertEquals(dataPointCount,366, @"Expected 366 datapoints");


        dataPoints = [selectedDataSource allDataPointsWithStepConsolidation:1];
        sortedDatesArray = [dataPoints allKeys];        
        // get the best consolidation
        NSDictionary* dataPoints2 = [selectedDataSource allDataPointsWithBestStepsGranularity];
        int dataPointCount2 = [dataPoints2 count];
        STAssertEquals(dataPointCount2, 755, @"Expected 755 datapoints");
        // check that the points from the previous set are still the same
        NSArray* sortedDatesArray2 = [dataPoints2 allKeys];
        // check that there are some values for the other points
        NSNumber *origval = [dataPoints objectForKey:[sortedDatesArray objectAtIndex:0]];
        NSNumber *finalval = [dataPoints2 objectForKey:[sortedDatesArray2 objectAtIndex:0]];
        STAssertEquals(origval, finalval, @"Expected data from consolidate 1 to be same as in best consolidate for same time period");
    }
    @catch (NSException *exception) {
        STFail(@"Failed to load database fron file");
    }
}

-(void) testXenHostFileXport{
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString* currentPath = [manager currentDirectoryPath];    
    NSString* filePath = [NSString stringWithFormat:@"%@/RoundRobinDatabaseTests/rrd_updates.xml", currentPath];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    @try {
        XportDatabase* db = [[XportDatabase alloc] initWithData:myData];
        NSArray* dataEntries = [db dataEntries];
        int dataEntriesCount = [dataEntries count];
        STAssertEquals(dataEntriesCount, 37, @"Expected 37 entries");
        
        DataEntry* entry = [dataEntries objectAtIndex:0];
                            
        NSString* name = [entry name];
        STAssertEqualObjects(name, 
                             @"AVERAGE:host:8227c277-ff26-4414-81a7-bff2f7a66be3:memory_total_kib",
                             @"Expected entry name is wrong");
        

        STAssertEqualObjects([entry valueType], 
                             @"AVERAGE",
                             @"Expected entry value type to be AVERAGE");

        STAssertEqualObjects([entry objectType], 
                             @"host",
                             @"Expected entry object type to be host");

        STAssertEqualObjects([entry objectId], 
                             @"8227c277-ff26-4414-81a7-bff2f7a66be3",
                             @"Expected entry objectId to be 8227c277-ff26-4414-81a7-bff2f7a66be3");

        STAssertEqualObjects([entry propertyDescription], 
                             @"memory_total_kib",
                             @"Expected entry propertyDescription to be memory_total_kib");        
        
        NSArray* values = [[dataEntries objectAtIndex:2] values];
        int valuesCount = [values count];
        STAssertEquals(valuesCount, 16, @"Expected 16 values in each entry");        
    }
    @catch (NSException *exception) {
        STFail(@"Failed to load database fron file");
    }
}

@end
