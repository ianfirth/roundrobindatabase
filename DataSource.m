//
//  DataSource.m
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "DataSource.h"
#import "DataPoint.h"
#import "OrderedDictionary.h"

@implementation DataSource
@synthesize name,dsType,alternateName;

- (id)initWithName:(NSString*)thisName 
               Type:(NSString*)thisStringType
         ForDatabase:(RoundRobinDatabase*)db
{
    self = [super init];
    if (self) {
        name = [thisName copy];
        [self setAlternateName:name];
        dsType = DS_Unknown;
        if ([thisStringType isEqualToString:@"GAUGE"]){
            dsType = DS_Gauge;
        }
        else if ([thisStringType isEqualToString:@"DERIVE"]){
            dsType = DS_Derive;
        }
        else if ([thisStringType isEqualToString:@"COUNTER"]){
            dsType = DS_Counter;
        }
        else if ([thisStringType isEqualToString:@"ABSOLUTE"]){
            dsType = DS_Absolute;
        }
        _dataPointDictionary = [[NSMutableDictionary alloc] init];
        _db = db;
    }
    
    return self;
}

- (NSArray*) dataPointsForConsolodatedStepCount:(int)consolodatedStepCount{
    NSNumber* num = [NSNumber numberWithInt:consolodatedStepCount];
    return [_dataPointDictionary objectForKey:num];
}

- (NSArray*) availableSteps{
    NSArray* keys = [_dataPointDictionary allKeys];
    NSArray* sortedDatesArray = [keys sortedArrayUsingComparator: ^(id a, id b) { 
        return [a compare: b]; 
    }];
    return sortedDatesArray;
}

- (NSDictionary*) allDataPointsWithBestStepsGranularity{
    OrderedDictionary* finalDictionary = [[OrderedDictionary alloc] init];
    // start with now as the oldest date
    // RRDs are not going to contain furtuistic data :)
    NSDate *oldeDatest = [NSDate date];  
             
    for (NSNumber* step in [self availableSteps]){
        NSDictionary* stepDictionary = [self allDataPointsWithStepConsolidation:[step intValue]];
        NSArray* sortedDatesArray = [stepDictionary allKeys];

        if ([finalDictionary count] == 0){
            // add initial set
            [finalDictionary addEntriesFromDictionary:stepDictionary];
            oldeDatest = (NSDate*)[sortedDatesArray lastObject];
        }
        else{
            // earliest date in current dictionary is the first one due to allKeys default sorting
            // go through everyentry that is before the last date of the previous one
            for (NSDate* date in sortedDatesArray){
                DataPoint* value = [stepDictionary objectForKey:date];
                if ([date compare:oldeDatest] == NSOrderedAscending){
                    oldeDatest = date;
                    [finalDictionary setObject:value forKey:date];
               }
            }
        }
    }
    return finalDictionary;
}

- (NSDictionary*) allDataPointsWithStepConsolidation:(int)stepConsolidation{
    OrderedDictionary* results = [[OrderedDictionary alloc] init];
    NSArray* points = [_dataPointDictionary objectForKey:[NSNumber numberWithInt:stepConsolidation]];
    NSDate* dateStart = [_db lastUpdate];
    for (DataPoint* dataPoint in points){
        [results setObject:dataPoint forKey:dateStart];
        // increment the date start
        int increment = [_db step] * stepConsolidation;
        dateStart = [dateStart dateByAddingTimeInterval:-increment];   
    }
    return (NSDictionary*)results;
}

-(void) dealloc{
    _dataPointDictionary = nil;
    _db = nil;
}

@end
