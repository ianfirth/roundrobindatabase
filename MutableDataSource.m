//
//  MutableDataSource.m
//  RoundRobinDatabase
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "MutableDataSource.h"

@implementation MutableDataSource

- (id)initWithName:(NSString*)thisName 
              Type:(NSString*)thisStringType 
    ForDatabase:(RoundRobinDatabase*) db
{
    self = [super initWithName:thisName Type:thisStringType ForDatabase:db];
    return self;
}

- (void) addDataPoint:(DataPoint *)dataPoint forStepCount:(int)consolodatedStepCount{
    NSNumber* num = [NSNumber numberWithInt:consolodatedStepCount];
    NSMutableArray* points = [_dataPointDictionary objectForKey:num];
    if (points == nil){
        points = [[NSMutableArray alloc] init];
        [_dataPointDictionary setObject:points forKey:num];
    }
    
    [points addObject:dataPoint];
}

@end
