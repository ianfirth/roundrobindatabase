//
//  DataSource.h
//  RoundRobinDatabase
//
//  Created by Ian Firth on 03/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RoundRobinDatabase.h"

typedef enum {
    DS_Unknown,
    DS_Gauge,
    DS_Counter,
    DS_Derive,
    DS_Absolute
} DataSourceType;
@interface DataSource : NSObject{
    
    //dataPoints indexed by consolodation step
    NSMutableDictionary* _dataPointDictionary;
    
    RoundRobinDatabase* _db;
}

- (id)initWithName:(NSString*)thisName 
              Type:(NSString*)thisType
        ForDatabase:(RoundRobinDatabase*)db;

- (NSArray*) dataPointsForConsolodatedStepCount:(int)consolodatedStepCount;
- (NSDictionary*) allDataPointsWithBestStepsGranularity;
- (NSDictionary*) allDataPointsWithStepConsolidation:(int)stepConsolidation;

@property (readonly, strong) NSString* name;
@property (strong) NSString* alternateName;
@property (readonly) DataSourceType dsType;
@property (weak, readonly) NSArray* availableSteps;

//minimalHeartbeat;
//min;
//max;
//last_ds
//value
//unkown_sec

@end
