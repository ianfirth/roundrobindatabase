//
//  DataPoint.h
//  RoundRobinDatabase
//
//  Created by Ian Firth on 03/09/2011.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

@interface DataPoint : NSObject{
    @protected
    double min;
}

- (id)initWitValue:(double)val;

@property (readonly) double min;
@property (readonly) double max;
@property (readonly) double average;

@end
