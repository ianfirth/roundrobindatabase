//
//  RoundRobinDatabase.h
//  RoundRobinDatabase
//
//  Created by Ian Firth on 02/09/2011.
//  Copyright 2011 hypops All rights reserved.
//

@interface RoundRobinDatabase : NSObject<NSXMLParserDelegate>{
    // list of elements in the current tree path being parsed
    NSMutableArray *elementList;
    
    // value of current element
    NSMutableString *elementValue;
    
    // collection of keyValues object being built
    NSMutableDictionary* DataSourceObjectParams;

    //dataSources
    NSMutableArray* _dataSources;
    
    //current stepConsolidation
    int stepConsolodation;
    
    // current consolidation type
    NSString* consolidationType;
    
    // current database row counter within the current step consolidation
    int rowCounter;

    // current dataSource value counter
    int dataSourceValueCounter;
    
    //fulldataSet
    BOOL useFullDataSet;

}

- (id)initWithString:(NSString *)xmlString withFullDataSet:(BOOL)fullDataSet;
- (id)initWithUrl:(NSURL *)url  withFullDataSet:(BOOL)fullDataSet;
// designated initializer
- (id)initWithData:(NSData *)data  withFullDataSet:(BOOL)fullDataSet;

@property (readonly) int step;
@property (readonly, strong) NSDate *lastUpdate;
@property (weak, readonly) NSArray* dataSources;


@end
